package com.atlassian.asap.core.client.jersey;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.client.okhttp3.AsapInterceptor;
import com.atlassian.asap.core.client.okhttp3.AsapInterceptorException;
import okhttp3.Interceptor;
import okhttp3.Request;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class AsapInterceptorTest {

    private static final String AUDIENCE = "my-audience";
    private static final String ISSUER = "my-issuer";
    private static final String KEY_ID = "my-key-id";
    private static final String AUTHORIZATION_HEADER_VALUE = "some-value";

    @Rule
    public MockitoRule mockitoJunitRule = MockitoJUnit.rule();

    @Mock
    private AuthorizationHeaderGenerator authorizationHeaderGenerator;

    @Mock
    private Interceptor.Chain chain;

    private AsapInterceptor asapInterceptor;

    @Before
    public void setup() {
        asapInterceptor = new AsapInterceptor(
                JwtBuilder.newJwt()
                        .audience(AUDIENCE)
                        .issuer(ISSUER)
                        .keyId(KEY_ID)
                        .build(),
                authorizationHeaderGenerator);
    }

    @Test
    public void shouldAddHeaderIfDoesNotExist() throws Exception {
        Request originalRequest = new Request.Builder()
                .get()
                .url("http://localhost:1234/")
                .build();
        when(chain.request()).thenReturn(originalRequest);
        when(authorizationHeaderGenerator.generateAuthorizationHeader(any(Jwt.class))).thenReturn(AUTHORIZATION_HEADER_VALUE);

        asapInterceptor.intercept(chain);

        verify(chain).proceed(argThat(hasAuthorization(AUTHORIZATION_HEADER_VALUE)));
    }

    @Test
    public void shouldNotAddHeaderIfItExists() throws Exception {
        Request originalRequest = new Request.Builder()
                .get()
                .url("http://localhost:1234/")
                .header("Authorization", "existing-value")
                .build();
        when(chain.request()).thenReturn(originalRequest);

        asapInterceptor.intercept(chain);

        verify(chain).proceed(argThat(hasAuthorization("existing-value")));
        verifyZeroInteractions(authorizationHeaderGenerator);
    }

    @Test(expected = AsapInterceptorException.class)
    public void shouldThrowExceptionIfTokenCanNotBeGenerated() throws Exception {
        Request originalRequest = new Request.Builder()
                .get()
                .url("http://localhost:1234/")
                .build();
        when(chain.request()).thenReturn(originalRequest);
        when(authorizationHeaderGenerator.generateAuthorizationHeader(any(Jwt.class))).thenThrow(new CannotRetrieveKeyException(""));

        asapInterceptor.intercept(chain);
    }

    private static Matcher<Request> hasAuthorization(String expectedValue) {
        return new TypeSafeMatcher<Request>() {
            @Override
            protected boolean matchesSafely(Request request) {
                String value = request.header("Authorization");
                return value.equals(expectedValue);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Request with 'Authorization' header with value ").appendValue(expectedValue);
            }
        };
    }
}
