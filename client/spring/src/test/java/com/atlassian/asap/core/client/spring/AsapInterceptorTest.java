package com.atlassian.asap.core.client.spring;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;

import java.io.IOException;
import java.util.Collections;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AsapInterceptorTest {
    private static final String AUDIENCE = "my-audience";
    private static final String ISSUER = "my-issuer";
    private static final String KEY_ID = "my-key-id";
    private static final String AUTHORIZATION_HEADER_VALUE = "some-value";

    @Rule
    public MockitoRule mockitoJunitRule = MockitoJUnit.rule();

    @Mock
    private AuthorizationHeaderGenerator authorizationHeaderGenerator;
    @Mock
    private HttpRequest clientRequest;
    @Mock
    private ClientHttpRequestExecution clientHttpRequestExecution;

    private HttpHeaders httpHeaders = new HttpHeaders();
    private AsapInterceptor asapInterceptor;

    @Before
    public void setup() {
        when(clientRequest.getHeaders()).thenReturn(httpHeaders);
        asapInterceptor = new AsapInterceptor(
                JwtBuilder.newJwt()
                        .audience(AUDIENCE)
                        .issuer(ISSUER)
                        .keyId(KEY_ID)
                        .build(),
                authorizationHeaderGenerator);
    }

    @Test
    public void shouldAddHeaderIfDoesNotExist() throws Exception {
        when(authorizationHeaderGenerator.generateAuthorizationHeader(any(Jwt.class))).thenReturn(AUTHORIZATION_HEADER_VALUE);

        asapInterceptor.intercept(clientRequest, new byte[0], clientHttpRequestExecution);

        assertThat(httpHeaders.containsKey(HttpHeaders.AUTHORIZATION), is(true));
        verify(authorizationHeaderGenerator).generateAuthorizationHeader(any(Jwt.class));
        verify(clientHttpRequestExecution).execute(clientRequest, new byte[0]);
    }

    @Test
    public void shouldNotAddHeaderIfItExists() throws Exception {
        httpHeaders.put(HttpHeaders.AUTHORIZATION, Collections.singletonList("auth"));

        asapInterceptor.intercept(clientRequest, new byte[0], clientHttpRequestExecution);

        verify(authorizationHeaderGenerator, never()).generateAuthorizationHeader(any(Jwt.class));
        verify(clientHttpRequestExecution).execute(clientRequest, new byte[0]);
    }

    @Test(expected = IOException.class)
    public void shouldThrowIoExceptionIfTokenCanNotBeGenerated() throws Exception {
        when(authorizationHeaderGenerator.generateAuthorizationHeader(any(Jwt.class))).thenThrow(new CannotRetrieveKeyException(""));

        asapInterceptor.intercept(clientRequest, new byte[0], clientHttpRequestExecution);
    }

}