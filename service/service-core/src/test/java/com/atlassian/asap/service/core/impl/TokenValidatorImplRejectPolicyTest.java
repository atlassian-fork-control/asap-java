package com.atlassian.asap.service.core.impl;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Optional;

import static com.atlassian.asap.service.api.TokenValidator.Policy.REJECT;
import static com.atlassian.asap.service.api.ValidationResult.Decision.ABSTAIN;
import static com.atlassian.asap.service.api.ValidationResult.Decision.REJECTED;
import static java.util.Optional.empty;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class TokenValidatorImplRejectPolicyTest extends AbstractTokenValidatorImplTest {
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();
    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void noTokenReturnsAbstain() {
        tokenValidator.policy(REJECT);

        assertThat(tokenValidator.validate(empty()), result(ABSTAIN));

        verifyZeroInteractions(jwtParser, jwtValidator);
    }

    @Test
    public void wrongHeaderPrefixReturnsAbstain() throws Exception {
        tokenValidator.policy(REJECT);

        assertThat(tokenValidator.validate(Optional.of("Not a jwt")), result(ABSTAIN));

        verifyZeroInteractions(jwtParser, jwtValidator);
    }

    @Test
    public void invalidTokenReturnsAbstain() throws Exception {
        tokenValidator.policy(REJECT);
        when(jwtParser.determineUnverifiedIssuer(TOKEN)).thenReturn(empty());

        assertThat(tokenValidator.validate(HEADER), result(ABSTAIN));

        verify(jwtValidator, never()).readAndValidate(anyString());
    }

    @Test
    public void readableTokenReturnsRejected() throws Exception {
        tokenValidator.policy(REJECT);
        when(jwtParser.determineUnverifiedIssuer(TOKEN)).thenReturn(Optional.of(ISSUER));

        assertThat(tokenValidator.validate(HEADER), result(REJECTED, ISSUER));

        verify(jwtValidator, never()).readAndValidate(anyString());
    }
}
