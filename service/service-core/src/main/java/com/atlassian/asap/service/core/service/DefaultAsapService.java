package com.atlassian.asap.service.core.service;

import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.keys.publickey.PublicKeyProviderFactory;
import com.atlassian.asap.core.validator.JwtClaimsValidator;
import com.atlassian.asap.service.api.AsapService;
import com.atlassian.asap.service.api.AuthorizationBuilder;
import com.atlassian.asap.service.api.TokenValidator;
import com.atlassian.asap.service.core.impl.AuthorizationBuilderImpl;
import com.atlassian.asap.service.core.impl.TokenValidatorImpl;
import com.atlassian.asap.service.core.spi.AsapConfiguration;

import java.security.PublicKey;

import static java.util.Objects.requireNonNull;

/**
 * A default implementation for {@code AsapService} that takes its settings from an {@code AsapConfiguration}.
 */
public class DefaultAsapService implements AsapService {
    private final AsapConfiguration config;
    private final JwtClaimsValidator jwtClaimsValidator;
    private final KeyProvider<PublicKey> publicKeyProvider;
    private final AuthorizationHeaderGenerator authorizationHeaderGenerator;

    @SuppressWarnings("unused")
    public DefaultAsapService(AsapConfiguration config,
                              JwtClaimsValidator jwtClaimsValidator,
                              PublicKeyProviderFactory publicKeyProviderFactory,
                              AuthorizationHeaderGenerator authorizationHeaderGenerator) {
        this(config, jwtClaimsValidator, createPublicKeyProvider(config, publicKeyProviderFactory), authorizationHeaderGenerator);
    }

    @SuppressWarnings("WeakerAccess")
    protected DefaultAsapService(AsapConfiguration config,
                                 JwtClaimsValidator jwtClaimsValidator,
                                 KeyProvider<PublicKey> publicKeyProvider,
                                 AuthorizationHeaderGenerator authorizationHeaderGenerator) {
        this.config = requireNonNull(config, "config");
        this.jwtClaimsValidator = requireNonNull(jwtClaimsValidator, "jwtClaimsValidator");
        this.publicKeyProvider = requireNonNull(publicKeyProvider, "publicKeyProvider");
        this.authorizationHeaderGenerator = requireNonNull(authorizationHeaderGenerator, "authorizationHeaderGenerator");
    }

    @Override
    public AuthorizationBuilder authorizationBuilder() {
        return new AuthorizationBuilderImpl(config, authorizationHeaderGenerator);
    }

    @Override
    public TokenValidator tokenValidator() {
        return new TokenValidatorImpl(config, publicKeyProvider, jwtClaimsValidator);
    }

    private static KeyProvider<PublicKey> createPublicKeyProvider(AsapConfiguration config,
                                                                  PublicKeyProviderFactory publicKeyProviderFactory) {
        requireNonNull(config, "config");
        requireNonNull(publicKeyProviderFactory, "publicKeyProviderFactory");
        return publicKeyProviderFactory.createPublicKeyProvider(config.publicKeyRepositoryUrl());
    }
}

