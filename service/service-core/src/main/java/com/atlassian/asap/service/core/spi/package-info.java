/**
 * Supporting classes that the host application must supply in order to use the default
 * implementation classes in the service API.
 */
@FieldsAreNonNullByDefault
@ParametersAreNonnullByDefault
@ReturnTypesAreNonNullByDefault

package com.atlassian.asap.service.core.spi;

import com.atlassian.asap.annotation.FieldsAreNonNullByDefault;
import com.atlassian.asap.annotation.ReturnTypesAreNonNullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
