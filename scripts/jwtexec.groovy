#!/usr/bin/env groovy
@GrabResolver(name = 'atlassian-public', root = 'https://maven.atlassian.com/repository/public', m2Compatible = 'true')
@Grab('org.slf4j:slf4j-simple')
@Grab('com.atlassian.asap:asap-client-core')
import com.atlassian.asap.api.Jwt
@GrabResolver(name='atlassian-public', root='https://maven.atlassian.com/repository/public', m2Compatible='true')
@Grab('org.slf4j:slf4j-simple')
@Grab('com.atlassian.asap:asap-client-core')

import com.atlassian.asap.api.Jwt
import com.atlassian.asap.api.JwtBuilder
import com.atlassian.asap.core.client.http.AuthorizationHeaderGeneratorImpl

import java.time.Instant

void checkUsage() {
    if (getPrivateKeyPath() == null) {
        printUsageAndExit('Missing jwt.privateKeyPath, or the path does not exist');
    }
    if (getAudience() == null) {
        printUsageAndExit('Missing jwt.audience');
    }
    if (getIssuer() == null) {
        printUsageAndExit('Missing jwt.issuer');
    }
}

void printUsageAndExit(String message) {
    String helpMessage = '''
Usage:
    groovy \\
        -Djwt.audience=audience \\
        -Djwt.issuer=issuer \\
        [-Djwt.keyId=issuer/key] \\
        [-Djwt.privateKeyPath=/opt/jwtprivatekeys] \\
    jwtexec.groovy \\
        curl -X POST --data foo=bar http://localhost:8080/path/to/url

''';

    System.err.println(helpMessage + 'ERROR! ' + message);
    System.exit(1);
}

void printAndExit(String message) {
    System.err.println('ERROR! ' + message);
    System.exit(1);
}

String getPrivateKeyPath() {
    File privateKeysDirectory = new File(System.getProperty('jwt.privateKeyPath', '/opt/jwtprivatekeys'));
    if (privateKeysDirectory.isDirectory()) {
        return privateKeysDirectory.getAbsolutePath();
    } else {
        return null;
    }
}

String getAudience() {
    return System.getProperty('jwt.audience');
}

String getIssuer() {
    return System.getProperty('jwt.issuer');
}

String getValidKeyId() {
    String keyId = getValidKeyIdFromProperty();
    if (keyId != null) {
        return keyId;
    }

    keyId = getValidKeyIdFromIssuerDirectory();
    if (keyId != null) {
        return keyId;
    }

    printAndExit("No keys matching *.pem in issuer directory.");
}

String getValidKeyIdFromProperty() {
    return System.getProperty('jwt.keyId');
}

String getValidKeyIdFromIssuerDirectory() {
    File issuerDirectory = new File(getPrivateKeyPath() + '/' + getIssuer());
    String[] filenames = issuerDirectory.list();
    String keyFilename = findValidKeyFilename(filenames);
    if (keyFilename == null) {
        return null;
    }
    return getIssuer() + '/' + keyFilename;
}


String findValidKeyFilename(String[] filenames) {
    if (filenames == null) {
        return null;
    }

    for (String filename : filenames) {
        if (filename.endsWith('.pem')) {
            return filename;
        }
    }

    return null;
}

String generateAuthorization(Jwt jwt) {
    return AuthorizationHeaderGeneratorImpl
        .createDefault(URI.create('file://' + getPrivateKeyPath()))
        .generateAuthorizationHeader(jwt);
}

Jwt createJwt() {
    return JwtBuilder.newJwt()
        .audience(getAudience())
        .issuer(getIssuer())
        .keyId(getValidKeyId())
        .expirationTime(Instant.now().plusSeconds(30*60))
        .build();
}

List<String> buildArgs(String authorization) {
    List<String> execArgs = new ArrayList<>();
    execArgs.addAll(args);
    if (args.length == 0) {
        execArgs.add('echo');
    }
    if (execArgs[0] == 'curl') {
        execArgs.add(1, '-HAuthorization: ' + authorization);
    } else {
        execArgs.add(authorization);
    }
    return execArgs;
}

int runCommand(List<String> execArgs) {
    new ProcessBuilder()
        .command(execArgs)
        .inheritIO()
        .start()
        .waitFor();
}

checkUsage();

Jwt jwt = createJwt();
String authorization = generateAuthorization(jwt);
List<String> execArgs = buildArgs(authorization);
System.exit(runCommand(execArgs));
