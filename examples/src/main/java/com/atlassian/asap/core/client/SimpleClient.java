package com.atlassian.asap.core.client;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.InvalidTokenException;
import com.atlassian.asap.core.client.http.AuthorizationHeaderGeneratorImpl;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.charset.StandardCharsets;

/**
 * A simple client for demonstration purposes. It makes a single GET request to a URL with a JWT access token.
 */
public class SimpleClient {
    private static final Logger logger = LoggerFactory.getLogger(SimpleClient.class);

    private final String issuer;
    private final String keyId;
    private final String audience;
    private final AuthorizationHeaderGenerator authorizationHeaderGenerator;

    /**
     * @param issuer         identification of this client
     * @param keyId          key ID used to sign the outgoing requests
     * @param audience       audience of this client
     * @param privateKeyPath location of the private keys
     */
    public SimpleClient(String issuer, String keyId, String audience, URI privateKeyPath) {
        this.issuer = issuer;
        this.keyId = keyId;
        this.audience = audience;
        this.authorizationHeaderGenerator = AuthorizationHeaderGeneratorImpl.createDefault(privateKeyPath);
    }

    /**
     * Makes an authenticated request to get a resource.
     *
     * @param resourceServerUrl URL of the resource
     * @throws CannotRetrieveKeyException the private identified by keyId cannot be found
     * @throws InvalidTokenException      some other problem when signing the token
     */
    @SuppressFBWarnings(
            value = "RCN_REDUNDANT_NULLCHECK_WOULD_HAVE_BEEN_A_NPE",
            justification = "Java.io.InputStreamReader does redundant null check here")
    public void execute(URI resourceServerUrl) throws CannotRetrieveKeyException, InvalidTokenException {
        Jwt jwt = JwtBuilder.newJwt()
                .keyId(keyId)
                .audience(audience)
                .issuer(issuer)
                .build();

        logger.info("Making a GET request to {} using access token {}", resourceServerUrl, jwt);
        HttpGet httpGet = new HttpGet(resourceServerUrl);
        httpGet.setHeader("Authorization", authorizationHeaderGenerator.generateAuthorizationHeader(jwt));

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build();
             CloseableHttpResponse response = httpClient.execute(httpGet);
             InputStreamReader reader = new InputStreamReader(response.getEntity().getContent(), StandardCharsets.UTF_8)) {
            logger.info("Request completed status={} -- response body: {}",
                    response.getStatusLine().getStatusCode(), IOUtils.toString(reader));
        } catch (IOException ex) {
            logger.error("Error accessing resource server", ex);
        }
    }
}
