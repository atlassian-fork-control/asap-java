package com.atlassian.asap.core.exception;

/**
 * Thrown if a problem was encountered while signing a token.
 */
public class SigningException extends RuntimeException {
}
