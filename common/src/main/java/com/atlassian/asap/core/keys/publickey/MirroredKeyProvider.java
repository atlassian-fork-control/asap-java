package com.atlassian.asap.core.keys.publickey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.exception.PublicKeyNotFoundException;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.PublicKey;
import java.util.Iterator;
import java.util.List;

/**
 * Represents a set of repositories that host identical set of keys - i.e. they are mirrors of each other in terms of
 * key content. Each repository in the set is expected to hold a full set of keys, so failure to find a key in any one
 * can be treated as authoritative for the set.
 */
public class MirroredKeyProvider implements KeyProvider<PublicKey> {
    private static final Logger logger = LoggerFactory.getLogger(MirroredKeyProvider.class);

    private final List<KeyProvider<PublicKey>> mirrors;

    @VisibleForTesting
    MirroredKeyProvider(List<KeyProvider<PublicKey>> mirrors) {
        this.mirrors = ImmutableList.copyOf(mirrors);
    }

    /**
     * Create a mirrored key provider representing a given set of mirrored key repository providers.
     * If there is only one provider in the given set of mirrors, then return the provider instead of
     * wrapping it.
     *
     * @param mirrors list of mirrored key repository providers
     * @return the mirrored key provider if there are more than one mirrors, or a key provider when it is
     * the only provider in the set of mirrors.
     */
    public static KeyProvider<PublicKey> createMirroredKeyProvider(List<KeyProvider<PublicKey>> mirrors) {
        // this is an optimization to avoid wrapping the keyprovider
        // when there is only one keyprovider
        if (mirrors.size() == 1) {
            return mirrors.get(0);
        } else {
            return new MirroredKeyProvider(mirrors);
        }
    }

    @Override
    public PublicKey getKey(ValidatedKeyId validatedKeyId) throws CannotRetrieveKeyException {
        for (Iterator<KeyProvider<PublicKey>> keyProviderIterator = mirrors.iterator(); keyProviderIterator.hasNext(); ) {
            try {
                return keyProviderIterator.next().getKey(validatedKeyId);
            } catch (PublicKeyNotFoundException ex) {
                // Because they are mirrored, if the key is not present in one of them,
                // then it is absent in all of them
                throw ex;
            } catch (CannotRetrieveKeyException ex) {
                if (keyProviderIterator.hasNext()) {
                    logger.debug("Error communicating with a key provider, going to try next mirror", ex);
                } else {
                    logger.warn("Error communicating with all mirrored key providers: {}", mirrors, ex);
                    throw new CannotRetrieveKeyException("Error communicating with all key providers", ex);
                }
            }

        }
        throw new CannotRetrieveKeyException("There are no mirrors available");
    }

    public List<KeyProvider<PublicKey>> getMirrors() {
        return mirrors;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" + mirrors + '}';
    }
}
