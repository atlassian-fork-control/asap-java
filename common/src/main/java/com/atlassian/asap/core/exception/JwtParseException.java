package com.atlassian.asap.core.exception;

import com.atlassian.asap.api.exception.InvalidTokenException;

/**
 * Indicates that the JWT was not well-formed.
 */
public class JwtParseException extends InvalidTokenException {
    public JwtParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public JwtParseException(Throwable cause) {
        super(cause);
    }

    public JwtParseException(String reason) {
        super(reason);
    }
}
