package com.atlassian.asap.core.keys.privatekey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.keys.KeyReader;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.util.Objects;

/**
 * Reads private keys from the filesystem.
 */
public class FilePrivateKeyProvider implements KeyProvider<PrivateKey> {
    private static final Logger logger = LoggerFactory.getLogger(FilePrivateKeyProvider.class);

    private final File baseDirectory;
    private final KeyReader keyReader;

    /**
     * Creates a new instance of {@link FilePrivateKeyProvider}.
     *
     * @param baseDirectory the base directory in filesystem where private keys are stored
     * @param keyReader     the key reader to use for reading private keys
     */
    public FilePrivateKeyProvider(File baseDirectory, KeyReader keyReader) {
        this.baseDirectory = Objects.requireNonNull(baseDirectory);
        this.keyReader = Objects.requireNonNull(keyReader);
    }

    @Override
    public PrivateKey getKey(ValidatedKeyId validatedKeyId) throws CannotRetrieveKeyException {
        File file = new File(baseDirectory, validatedKeyId.getKeyId());
        logger.debug("Reading private key from file system: {}", validatedKeyId.getKeyId());

        try (Reader reader = new InputStreamReader(new FileInputStream(file), StandardCharsets.US_ASCII)) {
            return keyReader.readPrivateKey(reader);
        } catch (FileNotFoundException e) {
            logger.debug("Private key file path {} does not exist or is not a file", file);
            throw new CannotRetrieveKeyException("Private key file path does not exist or is not a file");
        } catch (IOException e) {
            throw new CannotRetrieveKeyException(String.format("Error retrieving private key from file: '%s'", file), e);
        }
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
