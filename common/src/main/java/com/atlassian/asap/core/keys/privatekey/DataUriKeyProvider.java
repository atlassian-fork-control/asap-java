package com.atlassian.asap.core.keys.privatekey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.keys.DataUriKeyReader;
import com.atlassian.asap.core.keys.DataUriUtil;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.validator.ValidatedKeyId;

import java.io.StringReader;
import java.net.URI;
import java.security.PrivateKey;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Key provider for keys embedded in data uri.
 *
 * @see DataUriKeyReader
 */
public class DataUriKeyProvider implements KeyProvider<PrivateKey> {
    static final String URI_SCHEME = "data";

    private final String dataUriKeyId;
    private final PrivateKey privateKey;

    public DataUriKeyProvider(URI dataUri, DataUriKeyReader dataUriKeyReader) {
        checkArgument(dataUri.isAbsolute(), "URI must be absolute"); // implies that scheme != null
        checkArgument(dataUri.isOpaque(), "URI must not have path components");
        checkArgument(URI_SCHEME.equals(dataUri.getScheme()), "URI must have data scheme");

        this.dataUriKeyId = DataUriUtil.getKeyId(dataUri.toString());
        this.privateKey = getKeyFromDataUri(dataUri, dataUriKeyReader);
    }

    @Override
    public PrivateKey getKey(ValidatedKeyId keyId) throws CannotRetrieveKeyException {
        if (dataUriKeyId.equals(keyId.getKeyId())) {
            return privateKey;
        } else {
            throw new CannotRetrieveKeyException("Unrecognized key id: " + keyId.getKeyId());
        }
    }

    private static PrivateKey getKeyFromDataUri(URI dataUri, DataUriKeyReader keyReader) {
        try (StringReader reader = new StringReader(dataUri.toString())) {
            return keyReader.readPrivateKey(reader);
        } catch (CannotRetrieveKeyException ex) {
            throw new IllegalArgumentException("Unable to parse key from data uri", ex);
        }
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
