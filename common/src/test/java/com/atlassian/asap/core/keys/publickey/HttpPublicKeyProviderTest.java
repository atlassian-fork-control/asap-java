package com.atlassian.asap.core.keys.publickey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.exception.PublicKeyNotFoundException;
import com.atlassian.asap.core.exception.PublicKeyRetrievalException;
import com.atlassian.asap.core.keys.PemReader;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.message.BasicHeader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HttpPublicKeyProviderTest {
    private static final BasicHeader CONTENT_TYPE_X_PEM_FILE = new BasicHeader("Content-Type", "application/x-pem-file");
    private static final URI BASE_URL = URI.create("https://example.test/");

    @Mock
    private PemReader pemReader;

    @Mock
    private HttpClient httpClient;

    @Mock
    private HttpResponse response;

    @Mock
    private HttpEntity entity;

    @Mock
    private StatusLine statusLine;

    @Mock
    private InputStream inputStream;

    @Mock
    private RSAPublicKey publicKey;

    @Captor
    private ArgumentCaptor<HttpGet> httpGetCaptor;

    private HttpPublicKeyProvider publicKeyProvider;

    @Before
    public void setUp() throws Exception {
        publicKeyProvider = new HttpPublicKeyProvider(BASE_URL, httpClient, pemReader);
        when(response.getEntity()).thenReturn(entity);
        when(response.getStatusLine()).thenReturn(statusLine);
        when(entity.getContent()).thenReturn(inputStream);

    }

    @Test(expected = PublicKeyRetrievalException.class)
    public void shouldFailIfHttpRequestFails() throws Exception {
        when(httpClient.execute(any(HttpGet.class))).thenThrow(new IOException("Error retrieving public key"));

        publicKeyProvider.getKey(ValidatedKeyId.validate("some-key-id"));
    }

    @Test(expected = PublicKeyRetrievalException.class)
    public void shouldFailIfKeyRepoReturnsInternalError() throws Exception {
        when(httpClient.execute(any(HttpGet.class))).thenReturn(response);
        when(response.getEntity()).thenReturn(null); // override default programming
        when(statusLine.getStatusCode()).thenReturn(500);

        publicKeyProvider.getKey(ValidatedKeyId.validate("some-key-id"));
    }

    @Test(expected = PublicKeyNotFoundException.class)
    public void shouldReturnNoneIfPublicKeyIsNotFound() throws Exception {
        when(httpClient.execute(any(HttpGet.class))).thenReturn(response);
        when(response.getEntity()).thenReturn(null); // override default programming
        when(statusLine.getStatusCode()).thenReturn(404);

        publicKeyProvider.getKey(ValidatedKeyId.validate("some-key-id"));
    }

    @Test(expected = CannotRetrieveKeyException.class)
    public void shouldFailIfPublicKeyDoesNotParse() throws Exception {
        when(httpClient.execute(any(HttpGet.class))).thenReturn(response);
        when(statusLine.getStatusCode()).thenReturn(200);
        when(entity.getContentType()).thenReturn(CONTENT_TYPE_X_PEM_FILE);
        when(pemReader.readPublicKey(any(Reader.class))).thenThrow(CannotRetrieveKeyException.class);

        publicKeyProvider.getKey(ValidatedKeyId.validate("some-key-id"));
    }

    @Test(expected = CannotRetrieveKeyException.class)
    public void shouldFailIfContentTypeIsNotPem() throws Exception {
        when(httpClient.execute(any(HttpGet.class))).thenReturn(response);
        when(statusLine.getStatusCode()).thenReturn(200);
        when(entity.getContentType()).thenReturn(new BasicHeader("Content-Type", "application/not-pem"));

        publicKeyProvider.getKey(ValidatedKeyId.validate("some-key-id"));
    }

    @Test
    public void shouldReturnParsedPublicKey() throws Exception {
        when(httpClient.execute(any(HttpGet.class))).thenReturn(response);
        when(statusLine.getStatusCode()).thenReturn(200);
        when(entity.getContentType()).thenReturn(CONTENT_TYPE_X_PEM_FILE);
        when(pemReader.readPublicKey(any(InputStreamReader.class))).thenReturn(publicKey);

        PublicKey pk = publicKeyProvider.getKey(ValidatedKeyId.validate("some-key-id"));
        assertEquals(publicKey, pk);
    }

    @Test
    public void shouldIncludeAcceptHeaderInTheRequest() throws Exception {
        when(httpClient.execute(any(HttpGet.class))).thenReturn(response);
        when(statusLine.getStatusCode()).thenReturn(200);
        when(entity.getContentType()).thenReturn(CONTENT_TYPE_X_PEM_FILE);
        when(pemReader.readPublicKey(any(InputStreamReader.class))).thenReturn(publicKey);

        publicKeyProvider.getKey(ValidatedKeyId.validate("some-key-id"));

        verify(httpClient).execute(httpGetCaptor.capture());
        assertEquals(HttpPublicKeyProvider.ACCEPT_HEADER_VALUE,
                httpGetCaptor.getValue().getFirstHeader(HttpHeaders.ACCEPT).getValue());
    }

    @Test
    public void shouldComposeTheBaseUrlAndTheKeyId() throws Exception {
        when(httpClient.execute(any(HttpGet.class))).thenReturn(response);
        when(statusLine.getStatusCode()).thenReturn(200);
        when(entity.getContentType()).thenReturn(CONTENT_TYPE_X_PEM_FILE);
        when(pemReader.readPublicKey(any(InputStreamReader.class))).thenReturn(publicKey);

        publicKeyProvider.getKey(ValidatedKeyId.validate("some-key-id"));

        verify(httpClient).execute(httpGetCaptor.capture());
        assertEquals(URI.create(BASE_URL + "some-key-id"), httpGetCaptor.getValue().getURI());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRejectSchemelessBaseUrl() throws Exception {
        new HttpPublicKeyProvider(URI.create("///bar/"), httpClient, pemReader);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRejectSchemesOtherThanHttps() throws Exception {
        new HttpPublicKeyProvider(URI.create("file:///bar/"), httpClient, pemReader);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRejectInsecureHttp() throws Exception {
        new HttpPublicKeyProvider(URI.create("http://example.test/"), httpClient, pemReader);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRejectBaseUrlWithoutTrailingSlash() throws Exception {
        new HttpPublicKeyProvider(URI.create("https://example.test/bar"), httpClient, pemReader);
    }

    @Test
    public void shouldAllowExtendingHttpClient() {
        // This test is a bit silly, but it shows an example use of the API (shrug).
        HttpRequestInterceptor interceptor = (request, context) -> {
            // do something here
        };
        HttpClient client = HttpPublicKeyProvider.defaultHttpClientBuilder()
                .addInterceptorFirst(interceptor)
                .build();
        new HttpPublicKeyProvider(BASE_URL, client, pemReader);
    }
}
