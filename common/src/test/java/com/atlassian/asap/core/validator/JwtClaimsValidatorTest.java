package com.atlassian.asap.core.validator;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.core.exception.InvalidClaimException;
import com.atlassian.asap.core.exception.TokenExpiredException;
import com.atlassian.asap.core.exception.TokenTooEarlyException;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class JwtClaimsValidatorTest {
    private static final String JWT_ID = "some-id";
    private static final String VALID_AUDIENCE = "valid-audience";
    private static final String VALID_AUDIENCE2 = "valid-audience-2";
    private static final String VALID_ISSUER = "valid-issuer";
    private static final String VALID_SUBJECT = "valid-subject";
    private static final Instant NOW = Instant.EPOCH; // arbitrary date
    private static final Instant THE_PAST = NOW.minus(1, ChronoUnit.DAYS);
    private static final Instant THE_FUTURE = NOW.plus(1, ChronoUnit.DAYS);

    private static final Jwt VALID_SELF_ISSUED_TOKEN = JwtBuilder.newJwt()
            .issuedAt(NOW)
            .notBefore(Optional.of(NOW))
            .expirationTime(NOW.plusSeconds(60))
            .keyId(VALID_ISSUER + "/some-key")
            .issuer(VALID_ISSUER)
            .audience(VALID_AUDIENCE)
            .jwtId(JWT_ID)
            .build();

    private JwtClaimsValidator validator;

    @Before
    public void createValidator() {
        validator = new JwtClaimsValidator(Clock.fixed(NOW, ZoneId.of("UTC+0")));
    }

    private void runValidate(Jwt jwt) throws Exception {
        validator.validate(jwt, ImmutableSet.of(VALID_AUDIENCE));
    }

    @Test
    public void shouldAcceptSelfIssuedValidClaims() throws Exception {
        runValidate(VALID_SELF_ISSUED_TOKEN);
    }

    @Test
    public void shouldAcceptJwtIfIssuerDoesNotMatchSubject() throws Exception {
        Jwt jwt = JwtBuilder.copyJwt(VALID_SELF_ISSUED_TOKEN)
                .subject(Optional.of(VALID_SUBJECT))
                .build();
        runValidate(jwt);
    }

    // Tests related to the audience, the subject and the issuer

    @Test(expected = InvalidClaimException.class)
    public void shouldRejectJwtIfIssuerIsBlank() throws Exception {
        Jwt jwt = JwtBuilder.copyJwt(VALID_SELF_ISSUED_TOKEN)
                .issuer("   ")
                .build();
        runValidate(jwt);
    }

    @Test(expected = InvalidClaimException.class)
    public void shouldRejectJwtIfKidDoesNotStartWithIssuer() throws Exception {
        Jwt jwt = JwtBuilder.copyJwt(VALID_SELF_ISSUED_TOKEN)
                .keyId("some-other/key")
                .build();
        runValidate(jwt);
    }

    @Test
    public void shouldAcceptJwtIfAudienceIsOneOfValidAudiences() throws Exception {
        Jwt jwt = JwtBuilder.copyJwt(VALID_SELF_ISSUED_TOKEN)
                .subject(Optional.of(VALID_SUBJECT))
                .build();
        validator.validate(jwt, ImmutableSet.of(VALID_AUDIENCE, VALID_AUDIENCE2));
    }

    @Test(expected = InvalidClaimException.class)
    public void shouldRejectJwtIfTheAudienceIsNotValid() throws Exception {
        Jwt jwt = JwtBuilder.copyJwt(VALID_SELF_ISSUED_TOKEN)
                .audience("invalid-audience")
                .build();
        runValidate(jwt);
    }

    @Test(expected = InvalidClaimException.class)
    public void shouldRejectJwtIfTheAudienceIsNotOneOfValidAudiences() throws Exception {
        Jwt jwt = JwtBuilder.copyJwt(VALID_SELF_ISSUED_TOKEN)
                .audience("invalid-audience")
                .build();
        validator.validate(jwt, ImmutableSet.of(VALID_AUDIENCE, VALID_AUDIENCE2));
    }

    // Formal verifications of time claims

    @Test(expected = InvalidClaimException.class)
    public void shouldRejectJwtThatWasIssuedImmediatelyAfterItsExpiry() throws Exception {
        Jwt jwt = JwtBuilder.copyJwt(VALID_SELF_ISSUED_TOKEN)
                .issuedAt(VALID_SELF_ISSUED_TOKEN.getClaims().getExpiry().plusSeconds(1))
                .build();
        runValidate(jwt);
    }

    @Test(expected = InvalidClaimException.class)
    public void shouldRejectJwtThatWasValidImmediatelyBeforeTheTimeItWasIssued() throws Exception {
        Jwt jwt = JwtBuilder.copyJwt(VALID_SELF_ISSUED_TOKEN)
                .notBefore(Optional.of(VALID_SELF_ISSUED_TOKEN.getClaims().getIssuedAt().minusSeconds(1)))
                .build();
        runValidate(jwt);
    }

    @Test(expected = InvalidClaimException.class)
    public void shouldRejectJwtThatIsNeverValid() throws Exception {
        Jwt jwt = JwtBuilder.copyJwt(VALID_SELF_ISSUED_TOKEN)
                .notBefore(Optional.of(VALID_SELF_ISSUED_TOKEN.getClaims().getExpiry().plusSeconds(1)))
                .build();
        runValidate(jwt);
    }

    @Test(expected = InvalidClaimException.class)
    public void shouldRejectJwtIfLifetimeExceedsOneHour() throws Exception {
        Jwt jwt = JwtBuilder.copyJwt(VALID_SELF_ISSUED_TOKEN)
                .expirationTime(NOW.plus(2, ChronoUnit.HOURS)) // long lived token
                .build();
        runValidate(jwt);
    }

    @Test
    public void shouldAcceptJwtIfLifetimeDoesntExceedsMaxLifetime() throws Exception {
        JwtClaimsValidator validator = new JwtClaimsValidator(Clock.fixed(NOW, ZoneId.of("UTC+0")), Duration.ofDays(30));
        Jwt jwt = JwtBuilder.copyJwt(VALID_SELF_ISSUED_TOKEN)
                .expirationTime(NOW.plus(29, ChronoUnit.DAYS))
                .build();
        validator.validate(jwt, ImmutableSet.of(VALID_AUDIENCE));
    }

    // Test that are relative to the current time

    @Test(expected = TokenExpiredException.class)
    public void shouldRejectJwtThatHasExpired() throws Exception {
        Jwt jwt = JwtBuilder.copyJwt(VALID_SELF_ISSUED_TOKEN)
                .expirationTime(THE_PAST)
                .issuedAt(THE_PAST) // because there is a formal validation that iat < exp
                .notBefore(Optional.<Instant>empty()) // because there is a formal verification that nfb < exp
                .build();
        runValidate(jwt);
    }

    @Test(expected = TokenTooEarlyException.class)
    public void shouldRejectJwtThatIsNotValidYetWhenUsingNotBefore() throws Exception {
        Jwt jwt = JwtBuilder.copyJwt(VALID_SELF_ISSUED_TOKEN)
                .issuedAt(THE_FUTURE)
                .notBefore(Optional.of(THE_FUTURE))
                .expirationTime(THE_FUTURE.plusSeconds(1)) // because there is formal validation that nbf < exp
                .build();
        runValidate(jwt);
    }

    @Test(expected = TokenTooEarlyException.class)
    public void shouldRejectJwtThatIsNotValidYetWhenNotUsingNotBefore() throws Exception {
        Jwt jwt = JwtBuilder.copyJwt(VALID_SELF_ISSUED_TOKEN)
                .issuedAt(THE_FUTURE)
                .notBefore(Optional.<Instant>empty())
                .expirationTime(THE_FUTURE.plusSeconds(1)) // because there is formal validation that nbf < exp
                .build();
        runValidate(jwt);
    }
}
