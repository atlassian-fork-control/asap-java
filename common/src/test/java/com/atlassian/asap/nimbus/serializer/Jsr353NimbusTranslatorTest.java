package com.atlassian.asap.nimbus.serializer;

import net.minidev.json.JSONObject;
import net.minidev.json.JSONStyle;
import org.junit.Test;

import javax.json.Json;
import javax.json.JsonObject;
import java.io.StringReader;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class Jsr353NimbusTranslatorTest {
    @Test
    public void shouldConvertJson() throws Exception {
        String originalJson = "{\"key\":[42,3.14,\"foobar\",true,false,null,{}]}";
        JsonObject originalJsr353 = Json.createReader(new StringReader(originalJson)).readObject();
        JSONObject convertedNimbus = (JSONObject) Jsr353NimbusTranslator.jsr353ToNimbus(originalJsr353);

        // it is easier to convert the string representations
        assertThat(JSONObject.toJSONString(convertedNimbus, JSONStyle.NO_COMPRESS), is(originalJson));
    }
}
