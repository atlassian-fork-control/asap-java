package com.atlassian.asap.it;

import com.sun.jersey.core.spi.component.ComponentContext;
import com.sun.jersey.core.spi.component.ComponentScope;
import com.sun.jersey.core.spi.component.ioc.IoCComponentProvider;
import com.sun.jersey.core.spi.component.ioc.IoCComponentProviderFactory;
import com.sun.jersey.core.spi.component.ioc.IoCFullyManagedComponentProvider;

import java.util.Map;

/**
 * Utility class to create IoCComponentProviderFactory instances.
 */
public final class JerseyTestUtil {

    private JerseyTestUtil() {
        // only static methods
    }

    /**
     * @param injectables map of injectable instances
     * @return an instance if IoCComponentProviderFactory which injects objects in injectables Map
     */
    public static IoCComponentProviderFactory ioCProviderFactoryfromMap(final Map<Class, Object> injectables) {
        IoCComponentProviderFactory ioCComponentProviderFactory = new IoCComponentProviderFactory() {
            @Override
            public IoCComponentProvider getComponentProvider(Class<?> c) {
                if (injectables.containsKey(c)) {
                    return wrap(injectables.get(c));
                }

                return null;
            }

            @Override
            public IoCComponentProvider getComponentProvider(ComponentContext cc, Class<?> c) {
                return getComponentProvider(c);
            }

            private IoCComponentProvider wrap(final Object instance) {
                return new IoCFullyManagedComponentProvider() {
                    @Override
                    public Object getInstance() {
                        return instance;
                    }

                    @Override
                    public ComponentScope getScope() {
                        return ComponentScope.Singleton;
                    }
                };
            }
        };

        return ioCComponentProviderFactory;
    }
}
