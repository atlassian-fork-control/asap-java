package com.atlassian.asap.core.server.jersey;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.server.http.RequestAuthenticator;
import com.atlassian.asap.core.server.AuthenticationContext;
import com.atlassian.asap.core.server.http.RequestAuthenticatorFactory;
import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.core.spi.component.ComponentContext;
import com.sun.jersey.core.spi.component.ComponentScope;
import com.sun.jersey.spi.inject.Injectable;
import com.sun.jersey.spi.inject.InjectableProvider;

import javax.ws.rs.ext.Provider;
import java.lang.reflect.Type;
import java.util.Objects;

@Provider
public class JwtAuthProvider implements InjectableProvider<JwtAuth, Type> {
    private final AuthenticationContext authContext;
    private final RequestAuthenticatorFactory requestAuthenticatorFactory;
    private final JerseyRequestAuthorizerFactory jerseyRequestAuthorizerFactory;

    public JwtAuthProvider(@InjectParam AuthenticationContext authContext,
                           @InjectParam RequestAuthenticatorFactory requestAuthenticatorFactory,
                           @InjectParam JerseyRequestAuthorizerFactory jerseyRequestAuthorizerFactory) {
        this.authContext = Objects.requireNonNull(authContext);
        this.requestAuthenticatorFactory = Objects.requireNonNull(requestAuthenticatorFactory);
        this.jerseyRequestAuthorizerFactory = Objects.requireNonNull(jerseyRequestAuthorizerFactory);
    }

    public JwtAuthProvider(@InjectParam AuthenticationContext authContext) {
        this(authContext, new RequestAuthenticatorFactory(), new JerseyRequestAuthorizerFactory());
    }

    @Override
    public Injectable getInjectable(ComponentContext componentContext, JwtAuth jwtAuth, Type type) {
        if (type.equals(Jwt.class)) {
            RequestAuthenticator requestAuthenticator = createRequestAuthenticator(authContext);
            JerseyRequestAuthorizer jerseyRequestAuthorizer = createRequestAuthorizer(jwtAuth);
            return new JwtInjectable(requestAuthenticator, jerseyRequestAuthorizer);
        } else {
            return null;
        }
    }

    private JerseyRequestAuthorizer createRequestAuthorizer(JwtAuth jwtAuth) {
        return jerseyRequestAuthorizerFactory.create(jwtAuth);
    }

    private RequestAuthenticator createRequestAuthenticator(AuthenticationContext authContext) {
        return requestAuthenticatorFactory.create(authContext);
    }

    @Override
    public ComponentScope getScope() {
        return ComponentScope.PerRequest;
    }
}
