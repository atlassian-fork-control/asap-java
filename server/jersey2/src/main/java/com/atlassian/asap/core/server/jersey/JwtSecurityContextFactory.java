package com.atlassian.asap.core.server.jersey;

import com.atlassian.asap.api.Jwt;

import javax.ws.rs.core.SecurityContext;

/**
 * Produces instances of {@link JwtSecurityContext}. Implementations of this interface can decide how to
 * build the {@link SecurityContext}, and in particular, the {@link java.security.Principal}.
 */
public interface JwtSecurityContextFactory {
    /**
     * @param jwt the authenticated {@link Jwt} token
     * @param previousSecurityContext the previous {@link SecurityContext}. Useful to extract properties.
     * @return a new instance of {@link JwtSecurityContext}
     */
    JwtSecurityContext createSecurityContext(Jwt jwt, SecurityContext previousSecurityContext);
}
