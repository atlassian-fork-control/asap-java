package com.atlassian.asap.core.server.filter;

import com.atlassian.asap.api.Jwt;
import com.google.common.collect.ImmutableMap;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * This class accepts a list of issuers and the authentication rules associated with each one.
 *
 * <p>Any issuer that is not in the list will be rejected.
 *
 * <p>For example, we might have some requirement that the issuer Mallory should only be able to use the subject "Mallory",
 * whereas the issuer Alice might be able to use the subject "Alice" or "Bob". We might write something like this:
 *
 * <pre>{@code
 *      rules = ImmutableMap.of(
 *          "Mallory", jwt -> "Mallory".equals(jwt.getClaims().getSubject().orElse("Mallory")),
 *          "Alice", jwt -> ImmutableSet.of("Alice", "Bob").contains(jwt.getClaims().getSubject().orElse("Alice"))
 *      );
 * }</pre>
 *
 * <p>This would prevent Mallory from using Alice or Bob as the subject when we don't expect them to. (This particular
 * example is actually covered better by the {@link IssuerAndSubjectAwareRequestAuthorizationFilter}, which extends this
 * class.)
 */
public class RulesAwareRequestAuthorizationFilter extends AbstractRequestAuthorizationFilter {
    private final Map<String, Predicate<Jwt>> issuersAndChecks;

    public RulesAwareRequestAuthorizationFilter(
            Map<String, Predicate<Jwt>> issuersAndChecks) {
        this.issuersAndChecks = ImmutableMap.copyOf(issuersAndChecks);
    }

    @Override
    protected boolean isAuthorized(HttpServletRequest request, Jwt jwt) {
        String issuer = jwt.getClaims().getIssuer();

        return Optional.ofNullable(issuersAndChecks.get(issuer))
                .map(pred -> pred.test(jwt))
                .orElse(false);
    }
}
