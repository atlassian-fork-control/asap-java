package com.atlassian.asap.core.validator;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.exception.InvalidTokenException;

public interface JwtRevalidator {

    /**
     * Re-Validates the claims of the decoded {@link Jwt} passed as input. This method does not  validate the signature
     * of the JWT, or that the public key belongs to the issuer.
     *
     * @param jwt the JWT to verify claims of
     * @throws InvalidTokenException if the claims are invalid or could not be verified
     */
    void revalidateClaims(Jwt jwt) throws InvalidTokenException;
}
