package com.atlassian.asap.core.server;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(properties = {
        "asap.audience=not-used",
        "asap.audience_override=aud1,aud2",
        "asap.public_key_repository.url=classpath:///"
})
@ContextConfiguration(classes = {AsapServerConfigurationIntegrationTest.TestContext.class, AsapServerConfiguration.class})
public class AsapServerConfigurationIntegrationTest {
    @Autowired
    private AsapServerConfiguration asapServerConfiguration;

    @Test
    public void shouldGetAllAudiences() {
        assertThat(asapServerConfiguration.getAllAudiences(), containsInAnyOrder("aud1", "aud2"));
    }

    static class TestContext {
        @Bean
        public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
            // required to expand the ${vars}
            return new PropertySourcesPlaceholderConfigurer();
        }
    }
}

