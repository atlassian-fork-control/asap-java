package com.atlassian.asap.core.server.filter;

import com.atlassian.asap.api.server.http.RequestAuthenticator;
import com.atlassian.asap.core.server.AuthenticationContext;
import com.atlassian.asap.core.server.http.RequestAuthenticatorFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;

import static com.atlassian.asap.core.server.filter.SpringRequestAuthenticatorSupplier.SPRING_CONTEXT_NAME_INIT_PARAM_KEY;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SpringRequestAuthenticatorSupplierTest {
    private static final AuthenticationContext AUTHENTICATION_CONTEXT = new AuthenticationContext("The Audience", "https://mock.invalid/");

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private FilterConfig filterConfig;
    @Mock
    private ServletContext servletContext;
    @Mock
    private WebApplicationContext springContext;

    private SpringRequestAuthenticatorSupplier supplier;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        setupMockSpringContext();

        supplier = new SpringRequestAuthenticatorSupplier(filterConfig);
    }

    private void setupMockSpringContext() {
        when(filterConfig.getServletContext()).thenReturn(servletContext);
        when(servletContext.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE)).thenReturn(springContext);
        when(springContext.getBean(AuthenticationContext.class)).thenReturn(AUTHENTICATION_CONTEXT);
    }

    @Test
    public void resolvesSpringContextWithCustomName() {
        when(filterConfig.getInitParameter(SPRING_CONTEXT_NAME_INIT_PARAM_KEY)).thenReturn("customSpringContext");
        SpringRequestAuthenticatorSupplier customContextSupplier = new SpringRequestAuthenticatorSupplier(filterConfig);
        verify(servletContext).getAttribute("customSpringContext");
    }

    @Test
    public void getSucceedsWithDefaultRequestAuthenticatorFactory() {
        when(springContext.getBean(RequestAuthenticatorFactory.class)).thenThrow(new NoSuchBeanDefinitionException("no such bean"));
        RequestAuthenticator requestAuthenticator = supplier.get();
        assertNotNull(requestAuthenticator);
    }

    @Test
    public void getSucceedsWithCustomRequestAuthenticatorFactory() {
        RequestAuthenticatorFactory customRequestAuthenticatorFactory = spy(new RequestAuthenticatorFactory());
        when(springContext.getBean(RequestAuthenticatorFactory.class)).thenReturn(customRequestAuthenticatorFactory);
        RequestAuthenticator requestAuthenticator = supplier.get();
        assertNotNull(requestAuthenticator);
        verify(customRequestAuthenticatorFactory).create(any(AuthenticationContext.class));
    }

    @Test
    public void getFailsIfAuthenticationContextIsMissing() {
        when(springContext.getBean(RequestAuthenticatorFactory.class)).thenThrow(new NoSuchBeanDefinitionException("no such bean"));
        when(springContext.getBean(AuthenticationContext.class)).thenThrow(new NoSuchBeanDefinitionException("no such bean"));
        expectedException.expectMessage("Unable to find unique bean");
        expectedException.expectMessage(AuthenticationContext.class.getName());

        supplier.get();
    }
}
