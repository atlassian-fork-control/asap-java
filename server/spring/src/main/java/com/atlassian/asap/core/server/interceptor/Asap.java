package com.atlassian.asap.core.server.interceptor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Asap is an annotation that will allow Jersey resource packages, classes, or methods to opt into ASAP authorization.
 */
@Target({ElementType.METHOD, ElementType.TYPE, ElementType.PACKAGE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Asap {
    /**
     * Only the subjects explicitly listed will be authorized.  If the subject isn't defined on an ASAP token, the
     * issuer value is used.  If the whitelist is empty, no authorization will take place.
     */
    String[] authorizedSubjects() default {};

    /**
     * Only the issuers explicitly listed will be authorized. If omitted/empty, the list of authorized issuers will be
     * equal to the list of authorized subjects ({@link #authorizedSubjects()}.
     */
    String[] authorizedIssuers() default {};

    /**
     * @return {@code true} if ASAP is enabled, {@code false} if it is disabled, applying no
     * authentication or authorization.
     * @since 2.6.1
     */
    boolean enabled() default true;

    /**
     * If {@code true} (which is the default) ASAP authentication is mandatory.
     *
     * <p>Keep {@code false} if <em>no</em> or <em>other</em> authentication forms are allowed for your
     * resource/service.
     *
     * @return {@code true} if ASAP is mandatory, {@code false} otherwise
     * @since 2.20
     */
    boolean mandatory() default true;
}
